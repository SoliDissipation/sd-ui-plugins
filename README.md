# sd-ui-plugins
plugins for https://github.com/cmdr2/stable-diffusion-ui

<h2>Script to help with spell (prompt) building</h2>
<ul>
<li>create individual spell tokens to rearrange and modulate by using commas</li>
<li>drag and drop tokens to rearrange order</li>
<li>hold ALT+scroll over a token to emphasize or de-emphasize</li>
<li>right click to delete a token</li>
<li>see a rough estimate for token count</li>
<li>enter link to a booru page to extract all tags from it, precede link with _ to replaces spaces with underscores in tags</li>
<li>recognize duplicate tags</li>
<li>add taglist.csv and taglist_sfw.csv to the plugin folder to enable autocomplete (comma separated text file of tagname:post_count items)</li>
<li>For questions join the <a href="https://discord.gg/cW2QxWrAGp">Easy Diffusion Discord</a> and visit the Spell tokenizer's <a href="https://discord.com/channels/1014774730907209781/1053293386771222558">plugin thread</a></li>
</ul>

<h2>Demo</h2>
<img src="https://gitlab.com/SoliDissipation/sd-ui-plugins/-/raw/main/demo/spell-tokenizer-demo.gif">